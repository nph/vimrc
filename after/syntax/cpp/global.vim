
" Load up the doxygen syntax
let g:load_doxygen_syntax=1


" Highlight strings inside C comments
let c_comment_strings=1

