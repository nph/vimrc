

" TComment
unmap <leader>_s
unmap <leader>_n
unmap <leader>_a
unmap <leader>_b
unmap <leader>_r
unmap <leader>_i
unmap <leader>_<Space>
unmap <leader>_p
unmap <leader>__
" unmap <Space>p
" unmap <Space>ig // ident-guides

"cecutil (Align & drawit dependencies)
unmap <Space>rwp
unmap <Space>swp
