Plug 't9md/vim-choosewin'

nmap  é  <Plug>(choosewin)
nmap  <leader>é  :ChooseWinSwapStay
let g:choosewin_overlay_enable = 1

