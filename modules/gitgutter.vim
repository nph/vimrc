Plug 'safetydank/vim-gitgutter'

let g:gitgutter_enabled = 0
nmap gh <Plug>GitGutterNextHunk
nmap gH <Plug>GitGutterPrevHunk

