
if v:version < 705
    finish
endif

let g:ycm_key_invoke_completion = '<F12>'

Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }

