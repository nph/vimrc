Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeFind','NERDTreeToggle'] }

" NERD_Tree
nnoremap <S-F5> :NERDTreeToggle<return>
nnoremap <F5> :NERDTreeFind<return>

let g:NERDTreeShowHidden=1
let g:NERDTreeWinSize=45

