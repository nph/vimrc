Plug 'blueyed/bufkill.vim'

" disable bufkill mappings
let g:BufKillCreateMappings=0
