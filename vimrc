
" first launch run :PlugInstall

if has('vim_starting')
   set nocompatible
endif

if has("win32")
    " Required to manage correctly listchars and YouCompleteMe plugin
    set encoding=utf-8

    " Improve win32 vim font rendering (directx shall be available)
    set rop=type:directx,gamma:1.0,contrast:0.5,level:1,geom:1,renmode:4,taamode:1
    
    runtime win32_runtime_path
endif

" set the <leader>, usually "\"
let mapleader=" "
let maplocalleader=" "

autocmd! InsertEnter * let mapleader="§" | let maplocalleader="§"
autocmd! InsertLeave * let mapleader=" " | let maplocalleader=" "

"open as zip
au BufReadCmd *.odt,*.ott,*.ods,*.ots,*.odp,*.otp,*.odg,*.otg call zip#Browse(expand("<amatch>"))
"au BufReadCmd *.odt,*.ott,*.ods,*.ots,*.odp,*.otp,*.odg,*.otg call zip#Browse(expand(""))
"au BufReadCmd *.FCStd,*.FCStd1 call zip#Browse(expand(""))


call plug#begin('~/.vim/plugged')

for fpath in split(globpath('~/.vim/modules', '**/*.vim'), '\n')
    execute 'source' fpath
endfor

" Plug 'SearchComplete'
" Plug 'joshtch/matchit.zip'
" Plug 'vim-scripts/JavaScript-Indent'
"Plug 'joonty/vdebug'

call plug#end()

filetype plugin indent on


set number
set ruler
set hlsearch
set incsearch
set ignorecase
set smartcase
"set tw=79
set autoindent
set smartindent
set shiftwidth=4
set tabstop=4
set softtabstop=4
set shiftround
set smarttab
set expandtab
set nopreserveindent
set ttyfast
set laststatus=2
" set relativenumber
set gdefault
set wrap
set nrformats-=octal
set ttimeout
set ttimeoutlen=50
set autoread

set display+=lastline

set clipboard=unnamed

"autocmd! BufWritePost .vimrc source %
autocmd FileType c,cpp set colorcolumn=120
autocmd FileType python set colorcolumn=80

set list
set listchars=tab:▸-,trail:¬,nbsp:˽

" persistent-undo
if has("persistent_undo")
    set undodir=$HOME/.vim_undo
    set undofile
    set undolevels=1000
    set undoreload=10000
    silent! call mkdir(&undodir, "p")
endif

set wildmode=list:longest,full
set wildmenu
set wildignore+=*.so,*.swp,*.dylib,*.a,*.o
set wildignore+=*.pyc,*.pyo
set wildignore+=*.swo,*.zip,*.exe,*.dll
set wildignore+=__pycache__

" allow left/right keys to wrap to the previous/next line
" <,> for normal & visual mode
" [,] for for insert & replace mode
"set whichwrap=<,>,[,]

set backup
set backupdir=$HOME/.vim_backup
silent! call mkdir(&backupdir, "p")
set patchmode=".bak"

set viewdir=$HOME/.vim_views
silent! call mkdir(&viewdir, "p")

set directory=$HOME/.vim_swap
silent! call mkdir(&directory, "p")

" show matching bracket on insertion
set showmatch

" show information about selection in visual mode.
set showcmd

filetype on
filetype plugin indent on
syntax on


let g:molokai_original = 1

let macvim_skip_colorscheme = 1
set background=dark
colorscheme solarized

" manage viminfo saved data
" see ':h E526'
set viminfo='50,<5000
set history=5000

" allow backspacing over autoindent, line breaks, and start of insert
set bs=indent,eol,start

" enable mouse for all modes
set mouse=a

set visualbell

" minimal number of screen lines to keep above and below the cursor.
"set scrolloff=3
set scrolljump=5

" set the gui's font
if has("mac")
    set guifont=Menlo:h14
    if ! has('gui_running')
        set term=ansi
        " set term=mac-ansi
    endif
else
    set guifont=Menlo\ for\ Powerline:h10
endif

"removes gui toolsbar
set guioptions-=T
set guioptions+=c


" easyer indent shifting : keep selection after shift
vnoremap < <gv
vnoremap > >gv

" easy window navigation
" and stop the search pattern highlighting for C-L
nnoremap <C-H> <C-W>h
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l:nohlsearch<return>
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<CR>


" idiot proof mappings --------------------------------------------------------
" avoids creation of a '<' file
cabbrev w< w

" clist/llist navigation utils
if has("mac")
    nnoremap <S-F14> :clist<return>
    nnoremap <F13>   :cp<return>
    nnoremap <F14>   :cc<return>
    nnoremap <F15>   :cn<return>
    nnoremap <M-S-F14> :llist<return>
    nnoremap <M-F13>   :lp<return>
    nnoremap <M-F14>   :ll<return>
    nnoremap <M-F15>   :lnext<return>
endif
nnoremap <S-F8> :clist<return>
nnoremap <F7>   :cp<return>
nnoremap <F8>   :cc<return>
nnoremap <F9>   :cn<return>
nnoremap <M-S-F8> :llist<return>
nnoremap <M-F7>   :lp<return>
nnoremap <M-F8>   :ll<return>
nnoremap <M-F9>   :lnext<return>


" use 'ag' if available
if executable('ag')
    set grepprg=ag
    let g:grep_cmd_opts = '--line-numbers --noheading'
endif


"ycm \o/ YouCompleteMe
" let g:ycm_autoclose_preview_window_after_completion=1
" let g:ycm_extra_conf_globlist = ['~/dev/*','!~/*']


set spell
set spelllang=en_us

